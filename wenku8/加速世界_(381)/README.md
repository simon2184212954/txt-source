# novel

- title: 加速世界
- title_zh1:
- author: 川原砾
- illust:
- source: http://www.wenku8.com/book/381.htm
- cover: http://img.wkcdn.com/image/0/381/381s.jpg
- publisher: 电击文库
- date: 2019-08-19T00:00:00+08:00
- status: 连载中
- novel_status: 0x0700

## illusts


## publishers

- wenku8

## series

- name: 加速世界

## preface


```
无论时代如何进步，这世上总是少不了遭到霸凌的一方。肥胖的国中生春雪就是其中一名受害者。
只有玩着学校局域网络中设置的壁球游戏时，他才能得到不受打扰的平静。这款以虚拟角色挑战「速度」极限的游戏平凡无奇，但春雪却非常喜欢。
时值秋季，春雪在学校里过着一如往常的日子。因缘际会之下，他认识了有着全校第一美貌与气质的少女「黑雪公主」，却从此改变了他的人生。
透过少女传给他的一款神秘软件，春雪得知了「加速世界」的存在。从这一瞬间起，校内地位处于最底端的春雪，就成了保护公主的骑士「超频连线者」，
网络起家的超人气作家，终于以电击小说大赏<大赏>得主之姿正式出道!实力派作家笔下的未来青春娱乐小说极速登场!
```

## tags

- node-novel
- wenku8

# contribute

- Linpop
- Naztar
- 輕之国度
- 路過的湯
- 請給我西撒醬的SSR
- 負犬小說組
- unasf948
- zbszsr
- wdr550
- 十七
- 養老驢
- 血型N型
- 湯滿自溢
- 伊織
- Andromeda
- 鳶尾紫
- 倉崎楓子_
- 魔都狂氣聖黑貓
- Zの小屋輕小創組-叉子
- XxalexX
- 朽影
- bulbfrm
- 千木咲音
- 絢辻詞
- 楪祈
- 浦部葵
- 七夜
- watashi101
- 葉月零
- 哦☆賣糕的
- 折原まいる
- 深珀の瞳
- yanplaywow
- @defan752
- ZeHaffen掃圖組
- 真甜
- 鳴泣
- MV
- 黑貓

# options

## downloadOptions

- noFilePadend: true
- filePrefixMode: 4
- startIndex: 1

## textlayout

- allow_lf2: true

## wenku8

- novel_id: 381

# link

