『白狼之巢』是洞窟型的寶物殿。

原本銀月是擁有高度知性和社會性的魔物，有著聚集大規模群體在一個巢穴中共同生活的習性。

據說最盛時期可達千匹以上，巢穴也像是蟻穴般縱橫無盡、其面積達到了小規模村莊的程度。

然後，在銀月已經滅絕、變成了寶物殿的如今，那個巢穴仍然保持著先前的狀態。

在宛如地面突然裂開似的巨大寶物殿的入口。緹諾一邊躲在茂盛樹木的陰影中觀察著寶物殿，一邊發出了嘆息。

本來的話，像魔獸之巢這樣的地方，在巢穴入口處常有複數的銀月放哨。

但是，如今在入口處打轉的是有著真紅色毛皮的狼人。距離十多米也能感受到帶有獸臭的溫熱吐息。閃亮的瞳孔在黑暗中非常顯眼。

手中握著的白刃在夜空中朦朧月光的映照下，微微閃著光。

「喂喂、不僅僅是有劍。還有拿著弓和火器的傢伙在啊。」

「可惡、那個狼人看上去不僅僅只是強啊。Mana•material供給過剩了吧？這一帶發生了什麼？」

聽到用沉重聲音抱怨著的吉爾貝特，古萊古皺著眉頭觀察著那個幻影。

當充斥著整個世界的mana•material積聚到一定濃度時，就會產生寶物殿和幻影。
在此之上、由於某種理由濃度變得更高時，幻影或寶物殿會吸收那些mana•material，升華為更高一段level的存在。

獵人們將這種恐怖的反常現象稱之為『進化』。

進化並不是隨便就能發生的事情。

Mana•material通常會沿著通往世界各處的地脈循環流動。
由於不停流動的原因，每個地點積聚的mana•material應該是有其限度的。

一般來說，只有在地脈變動、環境變化等外界情況導致mana•material在某處的濃度過量增加時才會發生進化。

憑借著附近的寶物殿而獲得了強大國力的澤布爾迪亞帝國對地脈變動很敏感。
如果有那種預兆的話應該會通知獵人們的。但是並沒有聽到過環境變化相關的情報。

然而，已經像這樣在眼前就出現了數個預料之外高等級的幻影，不禁讓人想抱怨。

緹諾調整了一下呼吸，冷靜地進行戰力分析。

幻影的體型比之前預測的還要大一倍。
而且，跟理應四足行走的紅月相比，眼前的狼人是用兩足站立的。身高也要比紅月高一倍。

「原本，這個巢穴是以銀月的體型大小為基準建造的。如果變得像現在這麼大的話，在行動上應該會被限制。不論是飛還是跳應該都無法做到……」

「比起在廣闊場所戰鬥還不如索性進入巢穴嗎……但是，我沒有遠距離攻擊的手段……」

巢穴外有五體狼人聚集在一起。

全身覆蓋著堅固的鎧甲這點仍舊沒變，但武器各不相同。

三體持有著劍，一體是弓，還有一體拿著沒見過的長形火器。由於站位的原因，想要不被察覺地進入巢穴是不可能的。

考慮可能會被前後夾擊，所以無視它們而強行突入巢穴的選項也不行。

「救助對象在這裡面？這麼明顯地看著就糟糕的寶物殿他們還進去？」

「……寶物殿進化了的話，也能期待會發現什麼好的寶具吧」

寶物殿和幻影、以及寶具的出現是捆綁在一起的。
Mana•material濃度越高，寶具就越強力。

而且，沒有人氣這一點也是重要的因素。寶具可是先到先得。

「有持有遠距離攻擊手段的人嗎？」

對緹諾的發問，古萊古和露達對視了一下。

在這種情況下所指的遠距離攻擊，是指對那些身穿鎧甲的狼人多少會造成一些傷害的攻擊。

舉例來說露達雖然可以把短劍投擲出去，但對穿著鎧甲、而且還擁有厚重毛皮的狼人們來講這不算是有效攻擊。

看到陷入沉默的兩人，緹諾再一次感到了這個隊伍組成的失衡。

一般隊伍的話，會至少加入一個、擅長遠距離攻擊來應對此種狀況的隊員。

吉爾貝特將背著的劍拿到手中，稍稍將姿勢擺正了一點。

「沒辦法，我來打頭陣。先把拿弓和火器的傢伙打倒之後總有辦法的吧」

「……哈？笨蛋嗎？」

「就算沒有魔力，煉獄劍也比普通的劍要厲害。沒問題，這種情況——早就習慣了」

吉爾貝特的裝備是輕裝。
嵌入了輕薄金屬板的皮鎧是喜歡輕便的獵人們的通常選擇，但絕對不是擔任誘餌的人應該選擇的裝備。

雖說比身為盜賊職業而特化了裝備輕便性的緹諾和露達稍強點，但即使這樣也連盾牌都沒有。吉爾貝特是雙手持劍特化了攻擊的類型，實在沒法認為也能擔任誘餌。

面對五個對手還能說出不服輸的話語，確實顯示出這個少年對於此種狀況的習以為常。

「說起來，你、被稱作單人隊伍來著？」

不同獵人所擁有的才能差別很大。隊伍成員之間存在實力差是理所當然的事，特別是一直與較弱伙伴共同戰鬥的獵人，有著不顧別人自己先動手的傾向。
因為至今為止都是那樣一直戰勝過來的。在新組成的隊伍中也經常因此而產生不和。

緹諾盯著作勢要站起來的吉爾貝特。

「別隨便行事。雖說想死的話隨你，而且也是臨時隊伍，但我身為隊長就有讓全員生還的義務。」

「……哈？」

對預想外的發言，吉爾貝特睜大了眼睛。

從隊伍組成的過程以及相互的性格來考慮，緹諾會說出像是擔心吉爾貝特的話，令人意外。

真要說起來，如果是隊內最靈活的緹諾的話，即使以眾多狼人為對手也能輕鬆擺脫吧。

與常年共事的伙伴不同，這個隊伍的成員是臨時召集的。
對吉爾貝特的視線，緹諾皺了皺眉，然後說道。

「不會捨棄你們的。這次，master期待著我作為隊長會如何行動。讓你們一人不少地活著回去不如說是——最低條件」

緹諾非常明白獵人不能只是說漂亮話。
有時為了守護全體，不得不做出捨棄某個隊員的判斷。

但是，這次被要求的不是那種事。

即使是臨時隊伍，Master也不會給出不得不捨棄同伴的委託。

這就是『起始的足跡』的master，克萊伊•安德裡希的作風。

哪怕是未曾相識的陌生人組成的臨時隊伍——不，正因如此，此時此刻，是在考驗緹諾•謝依朵作為隊長的資質。

呼吸著入夜後冰涼的空氣，抑制住因臨近戰鬥而高揚的心跳。

隨後，緹諾回頭看向隊伍成員們，開口說道。

「裝備輕便的我和露達先出去引起對手注意。我們接受過對遠距離武器的回避訓練。趁那個空隙古萊古和吉爾貝特從後方對後衛組發動強襲。逼近過去的話火器和箭矢也沒什麼可怕的。」


§§§


啊—拜託了就算只有緹諾也好一定要活著啊。就算現在以其他隊員為盾也無所謂。

我一邊咬緊牙關，一邊躍向僅有暗淡月光照亮的夜空。

空氣之膜覆蓋住我全身。
憑借寶具外套產生的推進力，我如離弦之箭那樣飛向空中。
同時也像極了有去無回的箭。

控制著身體向暗影存在的方向前進。

轉瞬間就越過包圍著帝都的牆壁和巨大的城門，眼前是廣闊到無邊無際的平原，以及沒有路燈的街道。

雖然看到了如此美麗的景色，但我的心情只感覺想吐。

『夜天之暗翼』是外套型的寶具。

由宛若夜色般美麗的藏青色布料和領口處點綴著的白色寶玉製作而成的這個寶具，能夠賜予使用者飛行能力這樣的究極之力。

擁有飛行能力的寶具很稀少，人氣又高，因此價格也很高。在我的收藏品中也只有一個『夜天之暗翼』。但是這個寶具有好幾個嚴重的缺陷。

在前任持有者身上發生的『人體導彈事件』是一件令人悲傷的事故，也將這個寶具的有用性和危險性很好地展示了出來。

由於猛烈的推進力而一頭倒栽進天花板的獵人就那樣被天國召喚了。之後將優秀獵人置於死地的『夜天之暗翼』在廢棄前被我拿到了手。名副其實的缺陷品。

但是，毫無疑問可以飛向空中。

雖然無法精密地控制，而且相對於重力制御、推進力重視過頭了所以無法『懸浮』，還沒有制動功能，盡是些問題，但毫無疑問可以飛向空中。

而且，速度快的亂七八糟。完全不考慮安全性的速度。
既然是寶具，那麼應該存在著作為這個寶具的原型的物品。真想對最初設計這個的人說教一個小時。

我瞬間就飛過了即使是人外一般的獵人也要花費一小時以上移動的距離，就這樣進入了森林中。

如果是在地面上移動的話，由於遮蔽視野的茂盛樹木、無處下足的石子和樹枝樹葉會很耗費體力吧，但跟能在空中飛的我沒什麼關係。

對在高空高速飛行的我，森林中棲息的鳥和野獸們嘈雜著發出悲鳴。想悲鳴的是我這邊才對啊。

之後我在異常開闊的視野中，總算發現了目標的寶物殿。

沒有樹木的開闊場地。像是鏤空的大地般的巨大巢穴。這附近沒有其他洞穴型的寶物殿了，肯定就是這裡。

很快。讓人想自誇的快。這樣的話緹諾也應該還活著才對。

接下來就只剩下寶具沒有剎車這個問題了。

因此我咬緊牙關，將前進方向調整為斜下方，順勢飛入了巢穴中。
