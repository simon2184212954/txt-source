# CONTENTS

昏き宮殿の死者の王  
昏暗宫殿的死者之王  

作者： 槻影  



- :closed_book: [README.md](README.md) - 簡介與其他資料
- [含有原文的章節](ja.md) - 可能為未翻譯或者吞樓，等待圖轉文之類
- [待修正屏蔽字](%E5%BE%85%E4%BF%AE%E6%AD%A3%E5%B1%8F%E8%94%BD%E5%AD%97.md) - 需要有人協助將 `**` 內的字補上
- [格式與譯名整合樣式](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E6%98%8F%E3%81%8D%E5%AE%AE%E6%AE%BF%E3%81%AE%E6%AD%BB%E8%80%85%E3%81%AE%E7%8E%8B.ts) - 如果連結錯誤 請點[這裡](https://github.com/bluelovers/node-novel/blob/master/lib/locales/)
-  :heart: [EPUB](https://gitlab.com/demonovel/epub-txt/blob/master/syosetu/%E6%98%8F%E3%81%8D%E5%AE%AE%E6%AE%BF%E3%81%AE%E6%AD%BB%E8%80%85%E3%81%AE%E7%8E%8B.epub) :heart:  ／ [TXT](https://gitlab.com/demonovel/epub-txt/blob/master/syosetu/out/%E6%98%8F%E3%81%8D%E5%AE%AE%E6%AE%BF%E3%81%AE%E6%AD%BB%E8%80%85%E3%81%AE%E7%8E%8B.out.txt) - 如果連結錯誤 請點[這裡](https://gitlab.com/demonovel/epub-txt/blob/master/syosetu/)
- :mega: [https://discord.gg/MnXkpmX](https://discord.gg/MnXkpmX) - 報錯交流群，如果已經加入請點[這裡](https://discordapp.com/channels/467794087769014273/467794088285175809) 或 [Discord](https://discordapp.com/channels/@me)


![導航目錄](https://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=https://gitlab.com/novel-group/txt-source/blob/master/syosetu_out/昏き宮殿の死者の王/導航目錄.md "導航目錄")




## [文](00000_%E6%96%87)


### [第一卷](00000_%E6%96%87/00000_%E7%AC%AC%E4%B8%80%E5%8D%B7)

- [序章 儀式復活](00000_%E6%96%87/00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/00010_%E5%BA%8F%E7%AB%A0%20%E5%84%80%E5%BC%8F%E5%BE%A9%E6%B4%BB.txt)
- [第一章 活死人](00000_%E6%96%87/00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/00020_%E7%AC%AC%E4%B8%80%E7%AB%A0%20%E6%B4%BB%E6%AD%BB%E4%BA%BA.txt)
- [第二章 生存欲望](00000_%E6%96%87/00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/00030_%E7%AC%AC%E4%BA%8C%E7%AB%A0%20%E7%94%9F%E5%AD%98%E6%AC%B2%E6%9C%9B.txt)
- [第三章 光與暗與可憐死者](00000_%E6%96%87/00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/00040_%E7%AC%AC%E4%B8%89%E7%AB%A0%20%E5%85%89%E8%88%87%E6%9A%97%E8%88%87%E5%8F%AF%E6%86%90%E6%AD%BB%E8%80%85.txt)
- [第四章 決戰](00000_%E6%96%87/00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/00050_%E7%AC%AC%E5%9B%9B%E7%AB%A0%20%E6%B1%BA%E6%88%B0.txt)
- [第五章 王之器](00000_%E6%96%87/00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/00060_%E7%AC%AC%E4%BA%94%E7%AB%A0%20%E7%8E%8B%E4%B9%8B%E5%99%A8.txt)
- [尾聲 再起](00000_%E6%96%87/00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/00070_%E5%B0%BE%E8%81%B2%20%E5%86%8D%E8%B5%B7.txt)
- [後記](00000_%E6%96%87/00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/00080_%E5%BE%8C%E8%A8%98.txt)


## [WEB](00010_WEB)

- [Prologue：儀式による復活](00010_WEB/00010_Prologue%EF%BC%9A%E5%84%80%E5%BC%8F%E3%81%AB%E3%82%88%E3%82%8B%E5%BE%A9%E6%B4%BB.txt)
- [第一話：生ける死者](00010_WEB/00020_%E7%AC%AC%E4%B8%80%E8%A9%B1%EF%BC%9A%E7%94%9F%E3%81%91%E3%82%8B%E6%AD%BB%E8%80%85.txt)

