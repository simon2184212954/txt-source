收到伊斯特魯魔法學院實戰訓練結束的消息，傭兵公會取回了原有的平靜。

雖說是每年的慣例，但麻煩的活動結束了，明天開始又要面對平常的日課。
這時，有位傭兵來到了公會長的房間。

是不善與人交流又寡言的傭兵，拉薩斯。

「⋯⋯我要進去嘍。」
「哎呀～歡迎回來。所以呢？事情經過如何啊？拉薩斯小弟♪」
「⋯⋯不要叫我小弟⋯⋯護衛學生的工作平安結束了。」

和他對話的是史提拉城的傭兵公會分會長，擔任此處公會長的賽馮。

「然後呢？杰羅斯先生的實力如何啊？」
「⋯⋯老實說深不見底。擅長格闘戰到了簡直不像魔導士的程度。而且就算對手是魔物，也只能窺見他實力的一小部份。太驚人了。」
「是啊～畢竟他連我都能輕易的打發了，到底是何方神聖啊？」
「⋯⋯輕易的打發你？我記得你的等級有３１２，他該不會⋯⋯在這之上吧？」
「因為我和他交手過所以知道，他不知道該怎麼處置自己的力量。雖然可以靈活運用，但可能沒有使出全力戰鬥過吧？」

拉薩斯是以傭兵公會派出的監察官角色參加這次的護衛任務的。

由於他是經過磨練的一流傭兵，實戰經驗也很豐富，所以經常會以監察官的身分來參加這種重要的任務。除了監視往後會成為傭兵的未來的魔導士外，有時也會站在招募新人的立場。

順帶一提，他的別稱是「剛腕之拉薩斯」。屬於Ｓ級，過去曾和賽馮一起組隊，解決了各式各樣的委託，實力高強。

正因為他很了解賽馮的實力，才會對於賽馮被輕易打發一事感到難以置信。

「⋯⋯到了這種程度嗎。平常先不論，他戰鬥起來是個非常危險的男人。特別是處在賭上性命的環境下時，他的人格會大幅改變，變得非常好戰喔？」
「哎呀？莫非有雙重人格？這點我可沒注意到呢。」
「不⋯⋯真要說起來，應該是因為他深知魔物的危險性所以才會變得相當冷酷吧。實際上要打倒魔物時，他一瞬間就解決了。」
「哦～⋯⋯不過你應該不覺得他只有這樣吧？」
「是啊⋯⋯以感覺來說，有某種殘暴的氣息潛藏在他的身上。有時會令人背脊發涼。」
「既然拉薩斯你這麼說，那應該沒錯吧。真可怕呢～♪」

或許是因為有過那樣的境遇吧，拉薩斯的直覺意外的敏銳。

然而他敏銳的直覺判斷杰羅斯十分危險。

「⋯⋯你為什麼那麼高興？」
「這個嘛～大概是因為知道了自己還有可能變得更強吧？魔導士可以強到那種程度喔？你不覺得我們應該可以更上一層樓嗎？」
「⋯⋯我不否認。不過強到那種程度的人還算是人嗎？跟魔物簡直沒兩樣。」
「只要不與他為敵就沒事了。既然可以溝通，剩下的就是誠意問題了吧？要是沒來由的排斥他，這才真的會引起對決呢。」

要是因為對方是強者便排斥，根本無法去計算與他敵對時的風險。

杰羅斯的確很強，光用初級的魔法「火焰」就能把哥布林給燒光。他認真使用魔法的話，簡直難以想像會有多大的威力。

最重要的是他身邊有一定的交友圈。這就代表他跟那些只執著於強大的人不同，擁有可以接納他人的器量。

這是因為只拘泥於力量的人，必然會排拒他人，也有容易輕視他人的傾向。
以具有實力的人而言他相當危險，但作為一個人來看的話則是可以信任的對象。

不過就算是這樣，仍無法改變他是個麻煩存在的事實。

「哎呀，沒問題吧？畢竟有公爵大人牽制著他，他對當傭兵也沒什麼興趣的樣子。」
「⋯⋯如果是這樣就好了，但他要是認真行動起來，我可不知道狀況會變得怎麼樣喔？」
「是啊，不過你不覺得很想見識看看嗎？」
「⋯⋯我不這麼想。那肯定會變得很棘手吧。我不想被捲入麻煩事裡。」

拉薩斯在公會中擔任負責培育新人的教官。他是結了婚才轉來做這個工作的。
想要回到妻子身邊，擁有穩定收入的話，便不適合再以傭兵為業了。

不像他那粗礦的外表，他是個一心深愛著唯一一位妻子的忠誠男人。

「好了，工作也做完了，今天來我家喝酒如何？我妻子們也很歡迎你來喔？」
「不⋯⋯我要回去了。我想趕快回去看看我太太。而且在你家感覺靜不下來⋯⋯特別是你的妻子們。」
「是嗎？我覺得比起喜歡小女孩來說好多了啊？我待在拉薩斯家反而才覺得靜不下來呢。」
「⋯⋯⋯⋯」

賽馮喜歡不輸男性的強勢女性。簡單來說就是喜歡外表比較男孩子氣，身材也很健美的女性。

相較之下拉薩斯則是偏好瘦小又可愛的女性。以一般的說法來說就是容易受到蘿莉體型的女性吸引。
順帶一提，拉薩斯的太太有著不符實際年齡的幼兒體型，也就是俗稱的合法蘿莉。而且還是外觀非常可愛的矮人族。

拉薩斯並不是蘿莉控。但他不知為何總是被喜歡可愛幻想風格的女性給吸引，追求瘦小又可愛的女性變成了必然的結果。然而至今為止都因為他那嚴肅可怕的臉及雄壯的體格而被拒絶。

這樣的他是在幾個月前才結婚的，其實還處在新婚蜜月期。

「唉，硬是要約新婚的你來喝酒也太不解風情了呢。今天就讓你早點回去陪太太吧。」
「⋯⋯嗯。是說賽馮你有那麼多妻子，不會起爭執嗎？這我從以前開始就很在意了⋯⋯」
「會啊？不過這樣就能感覺到我真的被愛著呢♡非常刺激喔？」
「⋯⋯那是我不想了解的境界。」

比起那種多人爭執的場面，還是圓滿的家庭比較好。他完全不羨慕處在比自己更深世界的戰友。拉薩斯到現在還是無法掌握賽馮的個性。

不過男女關係非常深奧這點他倒是已經充分了解了。

他在這之後離開了分部長辦公室，為了見妻子而直奔家裡了。是個愛妻人士這點似乎和戰友是一樣的。

要說他唯一的煩惱，就是因為和外表有如少女的矮人族女性結了婚，導致公會內出現了懷疑他是個蘿莉控的傳聞。

拉薩斯也是個在各方面都很辛苦的人。


◇　◇　◇　◇　◇　◇　◇

回到史提拉城的大叔，早早便回到旅館休息，療癒旅途的疲憊。

伊莉絲和嘉內也同意這點而回去休息了，唯有雷娜在傍晚跑到了城裡。

杰羅斯事到如今已經不想去深究她上哪去了，雙手合十對同時成為犧牲者的少年們祈禱後便當作沒發生過這件事。

沒必要自己主動去介入這種麻煩事。他就這樣抱著不負責任的態度鑽進了被窩中。

隔天早上，一如往常的穿著灰色長袍的杰羅斯，和伊莉絲等人一同聚集在傭兵公會的食堂。

在那裡的是⋯⋯

「嗯呼呼呼♡年輕的孩子果然很棒呢～♪」

嘴上這麼說著，肌膚充滿光澤，看起來非常高興的雷娜。

結束了護衛工作的雷娜，似乎又完成了一項工作回到旅館來了。用一般的說法來說就是玩到早上才回來。

雷娜的心情好得不得了。

雖然她的行動有些問題，但沒人打算進一步追究。

因為說了也沒用。

「雖然把素材拿去賣錢花了一點時間，不過這下總算可以回桑特魯了。大叔接下來打算怎麼辦？既然沒什麼事要做了，我們是打算立刻回去啦。」
「我會在史提拉城多待一天喔？因為我想去這城裡的大圖書館查些資料。你們要回桑特魯的話，我是可以送你們到有碼頭的鎮上，怎麼樣？」
「⋯⋯還是算了吧。我已經不想再坐那個了。」
「我有同感⋯⋯坐那個感覺都要沒命了。」

嘉內和雷娜拒絶搭乘「哈里・雷霆十三世」

雖然正確來說，她們坐的是掛在機車後頭的拖車。

就算是以地球上的法定速度駕駛，對於生活在這個世界的嘉內等人來說還是快到要沒命了吧。

在文明程度上是以馬車為主流的這個世界，要她們去搭乘數百年後的交通工具實在太艱辛了。簡直像是叫住在亞馬遜叢林裡的原住民去搭雲霄飛車。

需要花上不少時間才能習慣。

「畢竟你們那樣就會暈車了嘛，早點動身回桑特魯城比較好。」
「伊莉絲你呢？要和杰羅斯先生一起留在這城裡嗎？」
「嗯。畢竟和叔叔在一起很安全，我也有點在意魔法學院裡都在上些怎樣的課。」

正如大家所知，伊莉絲和杰羅斯一樣是轉生者。

她理所當然的不清楚這世界的常識，需要從各式各樣的地方搜集情報。以前曾被盜賊給抓住這件事，也讓她感覺到自己太缺乏這個異世界的常識了。

所以她才打算待在實力堅強的杰羅斯身邊調查異世界的事情。情報愈多愈有利這點無論在現實中還是遊戲裡都是一樣的，所以她也想試著增加自己的選項吧。

「學生之外的人雖然不能把書帶出大圖書館，但一般人也可以進去閱覽，所以對於想查資料的人來說是個好地方呢。是說真的不需要我送你們嗎？你們搭船也要花錢吧？不會花光報酬嗎？」
「我們手上還有賣掉素材得來的錢，付得起回程的船費吧？我和雷娜慢慢的回去吧。」
「回得去嗎⋯⋯？跟雷娜小姐喔？」

三個人的視線都集中到雷娜身上。

畢竟她只要發現中意的少年就會忽然消失，讓人實在不覺得她們能順利的回去。

她肯定會消失到哪裡去，然後來不及搭上船吧。

「真、真失禮！我也會是看時間跟場合的好嗎？不要把我跟杰羅斯先生的姊姊混為一談！」
「『『是哪張嘴敢說這種話啊⋯⋯明明發現獵物就當機立斷，旁若無人又自我中心⋯⋯』』」
「就算是我，也不會和可愛的男孩們談情說愛到連回程的船費都花光啦！而且要是沒有杰羅斯先生，不就拿不到公爵家那邊的任務報酬了嗎。我會乖乖的啦。」
「『『這絶對是謊言！』』」

這瞬間，三個人的想法完美地同步了。

「⋯⋯之前你嘴上說沒錢，卻從旅館裡走出來了吧？和好幾個少年們一起⋯⋯」
「是你多心了。也有可能是杰羅斯先生你看錯了吧。我不記得我有去過那樣的地方。」
「『那你之前沒回來教會的時候呢？要是真的忘了，那些少年們也太可怜了⋯⋯』」

扯上與少年們之間的戀愛糾葛，就不能信任雷娜。畢竟她曾經在任務途中忽然消失，在那之後才被人看到她從旅館裡走出來的樣子。把錢交給她太危險了。

「嘉內小姐，這是船費。我會順便請我們家那三只雞當你們的護衛，還請你想辦法把雷娜小姐護送至桑特魯城。」
「⋯⋯了解。我會負起責任帶雷娜回去的。還是盡量避免犧牲比較好⋯⋯就讓我期待那三只的戰力吧。」
「還有，這個寶石給你作為以防萬一時的住宿費，還請小心千萬別讓雷娜小姐給搶去了。我可不希望這被她拿去當作開房間的錢啊。」
「⋯⋯責任重大啊。我真的能完成這個任務嗎？」
「你們兩個，太失禮了吧──！」

嘉內背下了某種重責大任。

雖然雷娜看到大叔和嘉內的互動非常憤愾，但這也是她自作自受吧。俗話說無風不起浪啊。

總是在玩火，不知道何時會被衛兵抓走的雷娜是沒有選擇權的。他們可是不安到了可以的話想用草席把雷娜包起來再放進箱子裡，以釘子封箱後再用鐵煉捆起來的程度。

「是說叔叔你想查些什麼啊？魔法還是藥劑的素材？啊！也有可能是武器或防具的素材吧～」
「主要是歷史喔。特別是和四神教以及邪神相關的⋯⋯我想驗證一下自己的假設有多少是正確的，而且根據狀況不同，我有可能會被捲入奇怪的事情裡呢。想盡可能地搜集情報啊。」
「啊～⋯⋯也是呢，對於叔叔來說這問題特別嚴重吧。畢竟你是『大賢者』。」
「『大賢者？騙人的吧！』」

「大賢者」──除了傳說中曾提到之外，至今仍未有人達到的魔導士境界。被世人認為在邪神戰爭中全數滅絶，如今只會在戲劇或故事中登場的夢幻職業。

可以巧妙的操控魔法，還能製作出魔法藥和特殊裝備的究極職業。要是大賢者的存在公諸於世，一定會有很多國家搶著延攬吧。

而且嘉內和雷娜不知道杰羅斯的職業，所以十分驚愕。

另一方面，大叔怨恨地瞪著伊莉絲。

「伊莉絲小姐⋯⋯這是侵犯個人隱私喔？我啊，沒打算公開自己的職業喔⋯⋯？」
「唔⋯⋯對不起。」

伊莉絲被瞪得瑟縮起來。相較之下雷娜和嘉內則是驚慌得手足無措。

目前這個世界中，「大賢者」這個職業的存在尚未被實際確認。

這職業頂多只殘存於傳說或是歷史傳承中，且都被描述為負責管束賢者們的魔導士中的管理職。大叔是認為這種職業是否真的存在這點十分可疑，可能只是幻想。不僅攻擊魔法，連回復魔法和錬金術，甚至精通藥學和工藝學等各式各樣的學問及技術，這以人類的一生來說是不可能辦到的。所以他覺得除了自己之外，這世界上應該沒有「賢者」或「大賢者」才對。

傳說也是凝聚了人們憧憬的產物，所以就算不是魔導士，對賢者和大賢者抱有憧憬的人仍不在少數。也可以說是一種信仰吧。所以魔導士中也有以成為賢者為目標的人，然而對大叔來講這只是徒增困擾罷了。他不希望他人對自己有過度的期待。

『這麼說來，在這個世界回復魔法雖然是神官在使用，但魔導士也能用吧。唉，既然我能用，沒道理其他的魔導士不能用就是了。雖然很想驗證看看，但回復魔法似乎被某個國家給獨佔了呢～正好，也順便在大圖書館裡調查一下關於回復魔法的事情吧。』

在「Sword And Sorcery」中，魔導士雖然也能學會回復魔法，但因為是遊戲，所以無法發揮像神官職業那樣的效果。「職業」的修正效果會成為枷鎖，使得魔法的成效不彰。也就是說就算是「大賢者」，也必然不擅長使用回復魔法。

在這個世界怎樣他就不清楚了，關於修正這一點也必須不斷調查驗證才行。

總之，看嘉內和雷娜吃驚的樣子，就這樣延續「大賢者」的話題似乎不是明智之舉。

要是繼續說下去，話不小心傳入了他人的耳中，那可就不是一句麻煩事能帶過的了。甚至有可能會引發戰爭。畢竟發現了傳說中的大賢者，各國應該會殺紅了眼，想把他拉入自己的旗下吧。

『關於「職業」的事情，得先封住嘉內小姐她們的嘴才行⋯⋯哎呀？』

他想拜託嘉內她們幫忙保密，可是看向兩人後，只見她們似乎一直吃驚到現在，還像是被拋上岸的鯉魚一樣張口結舌，魂不知飛到那去了。

伊莉絲在兩人面前揮了揮手，確認她們的狀況。

「『大⋯⋯大大、大大大大大⋯⋯』」

她們終於發出了聲音，卻說不出話來。傳說中的職業出現在眼前，這也是理所當然的吧。

「大◯彈？啊，也有可能是大◯魔龍？新的那個⋯⋯」
「伊莉絲小姐，那樣的話歌詞不太對吧？不過是說你連舊版的都知道嗎？你到底幾歳啊？」
「不是啦────！那是什麼東西啊！比起那個，伊莉絲！大叔他真的是⋯⋯」
「不小心說出來了⋯⋯這好像是不該說出來的事情。」
「哎呀，杰羅斯先生不想出名嗎？既然是大賢者，財富或名聲這種東西想要多少就有多少吧？」
「那是當然的吧⋯⋯我不想和國家大人物派來延攬我的人打交道啊。不僅麻煩，最糟的情況下還有可能會牽連到路賽莉絲小姐或伊莉絲小姐你們喔？這樣你們還想說出去嗎？如果曝光的話我會立刻逃走喔？」
「原來如此⋯⋯不希望周遭的人變成人質，就得瞞著大家呢。我了解了⋯⋯杰羅斯先生也很辛苦呢⋯⋯」

要是擁有大賢者（杰羅斯）當作國家的專屬魔導士的話，他一個人就相當於有一支軍隊的戰力。

況且大叔完全是規格外的生物。以戰力來看的話他是可以一個人與國家為敵的危險人物。也不難理解他會想要對外隱瞞自己的職業。

然而伊莉絲卻不小心泄露出來了。由於在遊戲中五個殲滅者全都是大賢者是非常有名的事，她便說溜了嘴。

「雖然是很單純的問題⋯⋯不過『賢者』或『大賢者』是這麼不得了的東西嗎？」
「咦？因為⋯⋯杰羅斯先生你達到了魔導的極限，得到了真理對吧？既然這是沒有任何人能抵達的頂點，那不就是件很厲害的事情嗎？」
「不著痕跡的勸誡，時而幫助英雄的頂尖魔導士。這不是任何人都很憧憬的傳說中的職業嗎？」
「反過來說，就是說些感覺很像樣的話來誆騙年輕人，在最帥的時機出場放魔法。在故事最高潮的時候，為了尚不成熟的勇者說著『老夫一定會死守住這裡的！你們快點往前走吧！』這種話然後負責死掉的角色吧？這個該說是職業嗎？只是會成為犧牲品的可怜人嘛。我才不想要這樣咧，為了不認識的閑雜人等犧牲這種事⋯⋯」

雖然杰羅斯對賢者的認知有些偏頗，然而在許多故事中，大多數的賢者都是擔任這樣的角色。

說什麼抵達了睿智的頂點，聽起來是很好聽啦，但簡單來說就是個只顧著研究的家裏蹲，一個無藥可救的瘋狂科學家。這種人怎麼可能會為了他人戰鬥。

就算真的出來戰鬥，那也只是為了確認自己的研究成果所作的實驗，沒有那種「為了他人犧牲奉獻自己」的高尚情操。

實際上杰羅斯就沒有什麼犧牲奉獻的精神。

「『『的、的確⋯⋯就算是大賢者也沒道理要為了他人付出一切啦⋯⋯』』」

然而嘉內她們似乎沒辦法接受的樣子。對奇幻世界的既有形象被大叔破壊，也使得伊莉絲被失落感給折磨著。這也是受到了傳說或故事的影響吧。

換到現實世界中，沒人能理解賢者是為了什麼而戰。

「唉，這件事還請你們保密⋯⋯萬一被人知道了會很麻煩的，要是這個國家的人有所行動的話，那時候⋯⋯」
「『那、那時候會怎樣？』」
「這個國家應該會從地圖上消失吧。我啊，最討厭被權力給蒙蔽雙眼，想要支使他人的傢伙了，最慘的情況下會爆發戰爭喔？」
「雖然這樣說，但你之前在小瑟雷絲緹娜家當家教對吧？你不是討厭掌權的人嗎？」
「我是討厭沉溺在慾望中的傢伙。會讓我想起那個爛透了的姊姊⋯⋯」

應該有很多人想要請大賢者指導吧，但是魔法貴族在現在的索利斯提亞魔法王國中握有很強的勢力，所以他們一定會毫不掩飾自己的慾望來接近杰羅斯吧。

這樣就無法避免衝突了，最後很有可能會演變為戰爭。

當然應該也有單純的想要達到魔導士頂點的人在，但以大叔的角度而言，學生有幾個人就夠了。

沒辦法，說這是名譽啊榮耀啊等等，以權勢壓人的傢伙實在太煩人了。

「⋯⋯好了，雖然話題偏了，但嘉內小姐你們打算什麼時候離開這裡？」
「啊⋯⋯根據旅館的布告欄上面張貼的公告來看，定期船好像會在今天傍晚時出發。我們要是不馬上離開這裡的話就來不及了。要是錯過就得等到明天，我們可沒有住宿費。」
「也是。就算現在趕過去，搭馬車也要花上半天，算上馬的休息時間要傍晚才會到⋯⋯哎呀，時間還滿趕的？還有，不是沒有住宿費，而是不想花上多餘的錢吧？」
「說到節省，但時間變得這麼趕之前雷娜小姐也沒節省時間，不知道跑去做什麼了呢⋯⋯皮膚看起來特別有光澤就是了⋯⋯」
「別問這種不解風情的事。少年和女人之間，只有名為肉慾的險路啊？」
「『『只有對雷娜（小姐）你來說是這樣吧！』』」

明明已經沒時間了，雷娜仍忠於自己的慾望。在她心中，只有少年與女人＝肉慾饗宴這樣的等式吧。而且她還是個少年至上主義者。

真是同情接下來得負責監視她的嘉內的辛勞。

「要是她不聽人說話，感覺快要消失的時候⋯⋯」
「就拜託咕咕們了。我一個人怕是無法壓制住雷娜吧⋯⋯」
「等等，這太過分了吧？」

一點都不過分。不如說以棘手程度而言，雷娜跟莎蘭娜是同一個等級的。沒扯上關係就不要緊，但從認識的人眼裡看來，無論哪邊都很麻煩。

「雖然是搭船，還是請小心一點。特別是船上如果有未成年的人，更是要特別注意吧。」
「我知道。我是不覺得雷娜有飢不擇食到這種地步，但為了保險起見，有可以壓制住她的戰力還是幫了大忙啊。」
「壓制？大家到底是用怎樣的眼光在看待我的啊！」

「飢不擇食的正太控。」（伊莉絲表示）

「會對小孩子燃起慾望的危險人物。」（大叔說）

「比起三餐更喜歡小孩（在性生活的意義上）。」（嘉內如是說）

「⋯⋯⋯⋯⋯⋯⋯⋯」

被人理解這件事真是有好有壊。

而她已經很清楚周遭的人是怎麼看待自己的了。

雖然這麼說，但他們也不認為雷娜會反省，總之她現在想著「喜歡少年有什麼不行？在戰場上年紀不小的大叔還不是會渴求美少年的屁股⋯⋯這不公平。」消極的鬧起了別扭。

不，她真的有變消極嗎？這仍讓人有些不安。

「⋯⋯嘉內小姐，真的沒問題嗎？那個⋯⋯已經是末期了喔？」
「只要沒有那個，她也是個滿可靠的傢伙啊⋯⋯但為什麼我只覺得不安呢？」
「唉，畢竟是雷娜小姐⋯⋯事到如今警告她，她也不會反省吧～畢竟那可是雷娜小姐喔？」

伊莉絲這句話充滿了說服力。

一個人被排擠在外的雷娜生著悶氣。不過這也是她自作自受。

在那之後過了一小時，嘉內和雷娜帶著烏凱它們離開了史提拉城。

雖然是題外話，但不用說也知道雷娜被碰巧經過的少年給勾走了。

她也理所當然的受到了烏凱它們的鐵拳制裁，被多娜多娜的帶上路。

雷娜的壊習慣果然非常根深柢固。


◇　◇　◇　◇　◇　◇　◇

「老師，就是這裡了。」
「唔哇～～～⋯⋯」
「這還真是厲害啊⋯⋯（簡直就像是聖母院）」

和嘉內她們分開後，杰羅斯和伊莉絲在瑟雷絲緹娜的帶路下來到了大圖書館。

這建築風格與其說是圖書館，不如說是教會或大聖堂的建築物，從外觀給人的不平衡感，便能明顯看出是在建造途中才臨時改建成圖書館的。

透過巨大的花窗玻璃照入的光線，使得圖書館內有種莊嚴的氣氛，醞釀出一種不像是公共場所會有的神聖感。

提供學生們讀書的閱覽區非常寬闊，但並列的書架更是壓倒性的多。讓人數不清這裡的藏書量到底有多少。

「真了不起⋯⋯不愧是收藏了國內所有書籍的地方。這數量真不得了呢⋯⋯雖然不知道內容有幾分是真的就是了。」
「叔叔，你說這種話好嗎？書很貴吧？內容的正確性依據出版的國家或地點，標準也不同不是嗎？」
「就是因為這樣內容才有可能會有所偏頗啊。有多少從不同觀點寫下的書被留存下來，以及能否從中得出真相又是不同的問題了。」

這個世界的書就算以一般觀點來看，也大多是配合戰勝國的立場來記載事件的，很難從中找出立場中立的紀錄。由於大多數都只記載了對勝者有利的事情，一些不需要的內幕幾乎都被省略了。

也就是說想要了解內幕，必須去理解敗者那一方的歷史，真要探究起來不管有多少時間都不夠用吧。

所以杰羅斯只把焦點放在這個世界的宗教成立經過上。

然後很快的就過了約兩個小時。

「四神教的勢力是在邪神戰爭後才抬頭的。以此為前提，便能判明現在式微的創生神教才是先存在的宗教。問題是為什麼四神會出現。」
「不是因為創生神不在了嗎？或是宗教戰爭？」
「但是我試著調查後，發現歷史上沒有發生過那樣的戰爭呢。簡直就像是創生神教的信眾都改去信四神教了一樣。雖然我也只是大概有這種感覺⋯⋯」
「也就是說創生神教變成了四神教？沒有發生任何衝突？這種事情有可能嗎？」

以前在索利斯提亞公爵家的書庫翻閱資料時，並沒有找到兩個宗派發生衝突的歷史資料。

可是依據現在查到的書籍記載，創生神教以邪神戰爭為契機而衰退，相對的四神教的勢力崛起，僅過了約兩百五十年便完全取代了創生神教。而且在那之前使用的創生神教神殿，似乎都接連轉變為四神教的神殿了。

明明是這樣，兩個教派之間卻沒發生過任何事，這怎麼想都很奇怪。

所謂四神教，是從距今約兩千五百三十七年前，突然出現的巨大生物在蹂躪世界時開始的。當時雖有相當高度的魔導文明，卻束手無策的被這生物給破壊了。根據記載，當時也存有戰車和戰鬥機這類現代兵器，然而這仍是場單方面的戰爭。最後這個生物被稱作「邪神」

在世界約七成的文明都已經崩壊時，火之女神「弗雷勒絲」、風之女神「溫蒂雅」、水之女神「阿奎娜塔」、大地之女神「蓋拉涅絲」，這四位女神降臨於創生神教的神殿中，將七神器與「召喚勇者」的魔法陣賜給了人類。

然後就在數度召喚勇者形成的人海戰術防衛下，終於封印了邪神，以四神降臨的神殿為中心拓展勢力的四神教則是成了稱作「梅提斯聖法神國」的大國，一直到了現在。

「四神教的教義中是說世界是由四神所創造的，但實際上是怎樣呢⋯⋯」
「邪神的存在也讓人摸不著頭緒耶？是一開始就存在這個世界上，還是從某個世界忽然出現的呢⋯⋯不知道祂的真實身分呢。」
「我在意的是『為什麼是封印？』這點吧。畢竟本來應該要打倒邪神的。說不定四神沒有能夠打倒邪神的力量呢。而且根據其他資料來看，『等級』的概念也是在大約這個時候出現的。這好像是被召喚來的勇者特有的東西，在當時有留下類似的紀錄，然而現在卻已經普及化了。這個世界的法則在兩千五百年內改變了？有可能發生這種事嗎？」
「勇者⋯⋯也不時有來自神的郵件的紀錄，簡直就像是遊戲嘛？」
「被叫到這個世界來的他們後來怎麼了呢？活下來的勇者們在那之後就沒出現在歷史上了。是被送回去了，還是被當作危險分子給處理掉了呢？」

這中間也包含了臆測的成分在，但綜合目前所搜集到的情報，只能得知這些事。

就算是書本上的記載，能調查到的資料也是有限的。此外也有因時代更迭而被竄改的案例。

而杰羅斯最在意的，就是手裡這本書上畫的遺跡。

那是因和梅提斯聖法神國的戰役而毀滅的王國的廢墟。雖然像是以一根柱子為中心所畫出的巨大魔法陣，但那看起來像是用來進行什麼儀式的設施。

看著刻在中間柱子上的魔法文字，杰羅斯成功的解讀了那段文字。

『如果這是事實，那就代表邪神的存在是⋯⋯唉，這個就再繼續調查吧。真有什麼狀況就問本人好了。』

他沒將不確定的事情說出口，只調查覺得有疑點的事情。

「老師為什麼要查閱古時歷史的資料啊？雖然我只是有這種感覺，但老師你看起來對四神教好像有什麼想法的樣子，是我多心了嗎？」

瑟雷絲緹娜至今為止都默默聽著他們的對話，針對她的問題，杰羅斯在心中「呵⋯⋯」地冷笑了一聲，面向她答道。

「當然是因為我看他們不順眼⋯⋯沒錯，一切我都看不順眼啊。」

雖然瑟雷絲緹娜不知道，但杰羅斯對於讓自己陷入這種處境的四神可說是恨之入骨。但他當然不是憎恨虔誠的信眾。

杰羅斯想知道的，只有「四神真的是神嗎？」這一點。了解四神真正的身分，是決定大叔往後行動的要素之一。

首先，要是不知道邪神是何方神聖，復活後有可能會引發慘劇。就算因為有想問的事情而讓邪神復活，他也不想看到世界滅亡的慘狀。所以他很慎重的在處理這件事。

唉，這種事他不會跟學生說就是了。

『雖然很在意四神的事，但稍微放鬆一下，來調查跟回復魔法有關的事情好了⋯⋯』

大叔一邊隨意蒙混過去，一邊走向還沒查看過的書架。

用毫無緊張感，輕快的腳步⋯⋯


◇　◇　◇　◇　◇　◇　◇

「船要出航嘍────────！」

船員的聲音響徹了被夕陽染紅的碼頭，還沒搬完貨的作業員們慌張地跑了起來。

塞尚城同時也是商船的轉運點，就算在傍晚時分也有許多的船只入港，或是要從這個城鎮前往其他的港口。

在大小不一的船只中，嘉內她們朝著一艘相較之下屬于中型的商船跑去。

「雷娜，跑快點！船會跑掉的。」
「說是這麼說，可是嘉內⋯⋯你知道我們要搭哪艘船嗎？」
「我記得是『超・健美號』對吧？不就是那艘黑中帶紅的船嗎？」

嘉內她們趕搭的船，船身經過防銹處理，呈現深紅褐色，但是船頭有個仿造拳頭打造而成的金色裝飾，非常強烈又鮮明的閃耀著。

船頭通常應該會加上女神像或是龍之類的雕刻做裝飾，而船名為了祈求不要發生意外，也都會取聖人或是女性的名字才對。

這艘實在稱不上普通的船無謂的醒目。更何況船員全都半裸著上身，實在讓人不敢恭維，可是嘉內她們已經沒有選擇的餘地了。

主要是錢包的問題，但是──

「⋯⋯我不想搭那艘船。很沒品耶。」
「要是沒搭上那艘船，到明天為止就沒有船可搭了。就算是那樣的船也是定期船啊！」
「船員們也全都是肌肉壯漢喔？而且還擺好了姿勢以奇怪的笑容看向客人，難道他們是健美選手嗎？」
「不，他們是船員吧？而且也有其他客人搭上船了啊。」
「⋯⋯大家都一臉很不情願的樣子。這也是當然，光看就覺得熱得難受。雖然也有看起來很高興的夫人，但感覺她是在看別的地方。」

簡直有種整艘船像是肱二頭肌的錯覺。

真的是艘非常雄壯的船。

「好了，趕快搭上去吧！」
「我不要。感覺好像會懷孕，好可怕。我們搭別的船吧。」
「我們哪有那種閑錢啊！雖然從大叔那裡拿到了寶石，可是根據風向，不知道要花上幾天才能到桑特魯喔？必須盡量減少開銷才行。」
「可是這不合我的興趣啊～全都是壯漢，而且還半裸著身體⋯⋯」
「別說任性話了。如果不是處在這種狀況下，我也不想搭！而且不管搭哪艘船，船員幾乎都是男的。就算對此不滿也拿這狀況沒轍吧？」

船員們優美的展現他們的胸大肌。

是艘連船員都很雄壯的船。

「咕咕。（真是完美的肌肉啊。）」
「咕咕咕咕。（嗯，看來他們經過了不少鍛鍊。吾等也不能輸給他們。）」
「咕咕咕。（在下從沒見過那樣的肌肉，太美了。）」

在咕咕們之間似乎頗受好評。畢竟這些雞也是肉體派的。對於經鍛鍊打造出的肉體，它們會毫不吝嗇的給予讚賞吧。

他們應該不是在回應咕咕的稱讚，但是船員們以燦爛的笑容擺出了健美先生奧利瓦的招牌動作。

「少囉嗦，趕快上船吧！我們沒有多餘的預算！」
「要是知道事情會變成這樣，昨晚忍耐一下，不要去和我的達令們互訴情衷就好了。貧窮真是罪過啊。」
「這根本不是貧窮的錯吧！你給我稍微自重點！」

雷娜小姐是個徹底忠於慾望的人。

「再說，像那種肌肉⋯⋯」

雷娜的話沒能繼續說完。

她的背脊閃過一道有如閃電般的衝擊。

「爺爺，我們要搭那艘船嗎？」
「對喔，雖然上面都是些有點⋯⋯不對，很奇怪的船員，不過要去梅卡哈瑪城，只能搭那艘船呢。」
「哦～我想要早點見到媽媽。」
「沒事的～只要再忍耐個兩三天就能見到她嘍。我們也買了很～多土產啊。」
「嗯！」

從雷娜身邊走過的老人，以及像是他孫子的幼小少年。

看到那少年的時候，雷娜一瞬間露出了獵人般的眼神，然而嘉內卻看漏了那個瞬間。

這是個小小的失誤。

以年齡來說那應該在雷娜的守備範圍外的，她的本能卻受到了某種衝動的支配。

「⋯⋯我搭。」
「等等，雷娜？你⋯⋯到剛剛為止都還那麼不情願的，這是怎麼了？」
「沒事。預算很重要呢，我下次也會多注意的。不說了，趕快上船吧。」
「喔⋯⋯（她這忽然是怎麼了？意外的老實。）」

儘管露出了有些狐疑的表情，嘉內仍爬上了梯子。

雷娜的態度讓她有些難以釋懷。

滿身肌肉的船員們擺出了展現腹部與大腿的姿勢表示「歡迎來到我們的船上」，並以笑容迎接她們踏上甲板。

儘管看來像野獸，但實際上非常的紳士。

這時嘉內還沒注意到，雷娜早已化為了盯上獵物的肉食猛獸──

沒過多久船便啟航，離開了塞尚城。

夕陽往西邊下沉，天空終於被夜幕籠罩。

在世界被夜晚的黑暗給包覆之時，船成了無處可逃的狩獵場。

名為雷娜的野獸的狩獵場──

「爺爺！惡魔！那裡有惡魔！」
「嗯？那裡沒有東西喔？你是不是睡迷糊了？」
「不是啦～那裡、那裡有個表情很可怕的女人⋯⋯一定是惡魔啦～！」
「你一定是累了吧，拿你沒辦法，過來吧。」
「⋯⋯嗯，嗯。」

幼童被祖父給抱著入睡了。

而事實上，的確有人在黑暗之中窺視著他們。

在兩人靜靜沉睡的昏暗房間深處，出現了一抹帶著喜悅之情，有如弦月般的笑。

恐怖之夜開幕了。